<?php get_header();?>

<div id="home-content">
    <?php get_template_part('/template-parts/home/homepage', 'banner');?>
    <?php get_template_part('/template-parts/home/homepage', 'section1');?>
    <?php get_template_part('/template-parts/home/homepage', 'section2');?>
    <?php get_template_part('/template-parts/home/homepage', 'section3');?>
    <?php get_template_part('/template-parts/home/homepage', 'section4');?>
</div>

<?php get_template_part('/template-parts/others/book', 'now');?>

<?php get_footer();?>