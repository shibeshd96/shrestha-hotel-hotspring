<?php /* Template Name: Rooms and Suites Page */ ?>

<?php get_header();?>

<div id="home-content">
    <?php get_template_part('/template-parts/rooms/roomspage', 'banner');?>
    <?php get_template_part('/template-parts/rooms/roomspage', 'section1');?>
    <?php get_template_part('/template-parts/rooms/roomspage', 'section2-rooms');?>
    <?php get_template_part('/template-parts/rooms/roomspage', 'section3');?>
</div>

<?php get_template_part('/template-parts/others/book', 'now');?>

<?php get_footer();?>