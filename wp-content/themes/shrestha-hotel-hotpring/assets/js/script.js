jQuery(document).ready(function() {

    // Adding custom css classes to 3rd party plugins
    $('.splw-lite-header-title').addClass("type-m1 type-uppercase mb-4 pb-3").removeClass('splw-lite-header-title'); // Weather Plugin
    $('span.cur-temp').addClass("type-h2").removeClass('cur-temp'); // Weather Plugin
    $('.emaillist input[type="email"]').addClass("type-h3"); // Newsletter Plugin

    // On Scroll From Top Function
    $(window).on('scroll', function() {
        if($(window).scrollTop() > 0){
            $('.site-logo').css('height', '50px');
            $('.site-logo .main-logo').css('opacity', '0'); $('.site-logo .alt-logo').css('opacity', '1');
            $('.navbar').css('padding', '10px 0px');
        }else {
            $('.site-logo').css('height', 'auto');
            $('.site-logo .main-logo').css('opacity', '1'); $('.site-logo .alt-logo').css('opacity', '0');
            $('.navbar').css('padding', '18px 0px');
        }
    });


    // AOS INIT
    AOS.init();
});