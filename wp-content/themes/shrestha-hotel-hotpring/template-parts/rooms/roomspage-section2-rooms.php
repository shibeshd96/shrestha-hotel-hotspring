<?php 
    $rooms = new WP_Query(
        array(
            'post_type' => 'room',
            'order_by' => 'date',
            'order' => 'ASC'
        )
    );
?>

<section id="rooms-section-2">
    <div class="container">
        <div class="row justify-content-center">
            <?php $delay=100; $delay2=200; if ($rooms->have_posts()) : while ($rooms->have_posts()) : $rooms->the_post(); ?>
                <div class="col-md-4 col-sm-6 col-12">
                    <!-- <a href="<?php //the_permalink();?>"> -->
                    <a href="#">
                        <div class="blog px-3">
                            <?php if(has_post_thumbnail()): ?>
                                <img class="w-100" src="<?php echo get_the_post_thumbnail_url();?>" data-aos="fade-up" data-aos-delay="<?php echo $delay;?>" data-aos-duration="900">
                            <?php else : ?>
                                <img class="w-100" src="<?php echo get_template_directory_uri();?>/assets/images/background/placeholder.png" data-aos="fade-up" data-aos-delay="100" data-aos-duration="900">
                            <?php endif; ?>
                            <div class="blog-info" data-aos="fade-up" data-aos-delay="<?php echo $delay2;?>" data-aos-duration="900"
                                data-aos-anchor-placement="bottom-bottom">
                                <span class="type-m2 type-uppercase mt-4 d-block"><?php echo get_field('sub_title_text');?></span>
                                <h5 class="py-2 type-h2"><?php echo the_title();?></h5>
                                <button class="btn-arrow"><i class="fas fa-chevron-right"></i></button>
                            </div>
                        </div>
                    </a>
                </div>
            <?php $delay=$delay+150; $delay2=$delay2+50; endwhile; endif; wp_reset_query(); ?>
        </div>
    </div>
</section>