<section id="rooms-section-1">
    <div class="container my-5">
        <?php if( have_rows('first_section') ): while( have_rows('first_section') ): the_row(); ?>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-12">
                    <h3 class="type-h3 text-center" data-aos="fade-up" data-aos-duration="900"><?php echo get_sub_field('description');?></h3>
                </div>
            </div> 
        <?php endwhile; endif; ?>
    </div>
</section>