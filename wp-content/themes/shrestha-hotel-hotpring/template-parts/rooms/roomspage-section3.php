<section id="rooms-section-3">
    <div class="container">
        <?php if( have_rows('third_section') ): while( have_rows('third_section') ): the_row(); ?>
            <div class="row">
                <div class="col-lg-2">
                    <h3 class="py-5 type-2" data-aos="fade-up" data-aos-delay="100" data-aos-duration="900"><?php echo get_sub_field('title_text');?></h3>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <span class="type-m2 type-uppercase" data-aos="fade-up" data-aos-delay="100" data-aos-duration="900"><?php echo get_sub_field('sub_title_text');?></span>
                    <div class="py-5 type-h3 amenities-list">
                        <?php echo get_sub_field('description');?>
                    </div>
                </div>
            </div>
        <?php endwhile; endif; ?>
    </div>
</section>