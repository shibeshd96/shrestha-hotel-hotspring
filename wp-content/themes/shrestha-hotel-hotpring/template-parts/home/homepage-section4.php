<section id="home-section-4">
    <div class="container py-5">
        <?php if( have_rows('fourth_section') ): while( have_rows('fourth_section') ): the_row(); ?>
            <div class="row">
                <div class="col-md-4 col-sm-6 col-12">
                    <?php $first_link = get_sub_field('first_link'); if( $first_link ) : ?>
                        <?php foreach( $first_link as $firstpost ): setup_postdata($firstpost); ?>
                            <a href="<?php the_permalink($firstpost);?>">
                                <div class="blog px-3">
                                    <?php if(has_post_thumbnail($firstpost)): ?>
                                        <img src="<?php echo get_the_post_thumbnail_url($firstpost, 'long-thumbnails');?>" data-aos="fade-up" data-aos-delay="100" data-aos-duration="900">
                                    <?php else : ?>
                                        <img src="<?php echo get_template_directory_uri();?>/assets/images/background/placeholder.png" data-aos="fade-up" data-aos-delay="100" data-aos-duration="900">
                                    <?php endif; ?>
                                    <div class="blog-info" data-aos="fade-up" data-aos-delay="200" data-aos-duration="900" data-aos-anchor-placement="bottom-bottom">
                                        <h5 class="pt-4 pb-3 type-h2"><?php echo get_the_title($firstpost);?></h5>
                                        <button class="btn-arrow"><i class="fas fa-chevron-right"></i></button>
                                    </div>
                                </div>
                            </a>
                        <?php break; endforeach; wp_reset_postdata(); ?>
                    <?php else : endif; ?>
                </div>
                <div class="col-md-4 col-sm-6 col-12">
                    <?php $second_link = get_sub_field('second_link'); if( $second_link ) : ?>
                        <?php foreach( $second_link as $secondpost ): setup_postdata($secondpost); ?>
                            <a href="<?php the_permalink($secondpost);?>">
                                <div class="blog px-3">
                                    <?php if(has_post_thumbnail($secondpost)): ?>
                                        <img src="<?php echo get_the_post_thumbnail_url($secondpost, 'long-thumbnails');?>" data-aos="fade-up" data-aos-delay="250" data-aos-duration="900">
                                    <?php else : ?>
                                        <img src="<?php echo get_template_directory_uri();?>/assets/images/background/placeholder.png" data-aos="fade-up" data-aos-delay="100" data-aos-duration="900">
                                    <?php endif; ?>
                                    <div class="blog-info" data-aos="fade-up" data-aos-delay="350" data-aos-duration="900" data-aos-anchor-placement="bottom-bottom">
                                        <h5 class="pt-4 pb-3 type-h2"><?php echo get_the_title($secondpost);?></h5>
                                        <button class="btn-arrow"><i class="fas fa-chevron-right"></i></button>
                                    </div>
                                </div>
                            </a>
                        <?php break; endforeach; wp_reset_postdata(); ?>
                    <?php else : endif; ?>
                </div>
                <div class="col-md-4 col-sm-6 col-12">
                    <?php $third_link = get_sub_field('third_link'); if( $third_link ) : ?>
                        <?php foreach( $third_link as $thirdpost ): setup_postdata($thirdpost); ?>
                            <a href="<?php the_permalink($thirdpost);?>">
                                <div class="blog px-3">
                                    <?php if(has_post_thumbnail($thirdpost)): ?>
                                        <img src="<?php echo get_the_post_thumbnail_url($thirdpost, 'long-thumbnails');?>" data-aos="fade-up" data-aos-delay="400" data-aos-duration="900">
                                    <?php else : ?>
                                        <img src="<?php echo get_template_directory_uri();?>/assets/images/background/placeholder.png" data-aos="fade-up" data-aos-delay="100" data-aos-duration="900">
                                    <?php endif; ?>
                                    <div class="blog-info" data-aos="fade-up" data-aos-delay="400" data-aos-duration="900" data-aos-anchor-placement="bottom-bottom">
                                        <h5 class="pt-4 pb-3 type-h2"><?php echo get_the_title($thirdpost);?></h5>
                                        <button class="btn-arrow"><i class="fas fa-chevron-right"></i></button>
                                    </div>
                                </div>
                            </a>
                        <?php break; endforeach; wp_reset_postdata(); ?>
                    <?php else : endif; ?>
                </div>
            </div>
        <?php endwhile; endif; ?>
    </div>
</section>