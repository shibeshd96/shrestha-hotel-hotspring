<?php if(has_post_thumbnail()): ?>
    <div id="banner" style="background-image: url('<?php echo get_the_post_thumbnail_url();?>')" class="position-relative">
<?php else : ?>
    <div id="banner" style="background-image: url('<?php echo get_template_directory_uri();?>/assets/images/background/banner_image_alt.jpg')" class="position-relative">
<?php endif; ?>

    <?php if( have_rows('banner_section') ): while( have_rows('banner_section') ): the_row(); ?>
        <div class="container">
            <div class="banner-content mt-5">
                <div class="row">
                    <span id="pre-title-text" data-aos="fade-right" data-aos-delay="700" data-aos-duration="800" class="type-m2 type-uppercase"><?php echo get_sub_field('pre_title_text');?></span>
                </div>
                <div class="row">
                    <span id="title-text" data-aos="fade-right" data-aos-delay="800" data-aos-duration="900" class="type-h1 type-uppercase"><?php echo get_sub_field('title_text');?></span>
                </div>
                <div class="row">
                    <span id="linked-news" data-aos="fade-right" data-aos-delay="900" data-aos-duration="1000">
                        <?php $linked_news = get_sub_field('linked_news'); if( $linked_news ) : ?>
                            <?php foreach( $linked_news as $post ): setup_postdata($post); ?>
                                <a href="<?php the_permalink();?>" class="type-m1 type-uppercase">
                                    <?php the_title();?> 
                                    <button class="btn-arrow"><i class="fas fa-chevron-right"></i></button>
                                </a>
                            <?php break; endforeach; wp_reset_postdata(); ?>
                        <?php else : endif; ?>
                    </span>
                </div>
            </div>
        </div>
    <?php endwhile; endif; ?>
    <div class="overlay"></div>
    <div class="white-overlay"></div>
</div>
