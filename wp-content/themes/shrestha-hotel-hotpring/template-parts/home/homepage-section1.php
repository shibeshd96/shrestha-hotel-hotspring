<section id="home-section-1">
    <div class="container my-5">
        <?php if( have_rows('first_section') ): while( have_rows('first_section') ): the_row(); ?>
            <div class="row">
                <div class="col-lg-6 col-md-12">
                    <h3 class="type-h2" data-aos="fade-up" data-aos-duration="900"><?php echo get_sub_field('title_text');?></h3>
                </div>
            </div> 
            <div class="row">
                <div class="col-lg-4 col-md-0"></div>
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <p class="type-h3" data-aos="fade-up" data-aos-delay="250" data-aos-duration="900"><?php echo get_sub_field('sub_text_one');?></p>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <p class="type-h3" data-aos="fade-up" data-aos-delay="400" data-aos-duration="900"><?php echo get_sub_field('sub_text_two');?></p>
                </div>
            </div>
        <?php endwhile; endif; ?>
    </div>
</section>