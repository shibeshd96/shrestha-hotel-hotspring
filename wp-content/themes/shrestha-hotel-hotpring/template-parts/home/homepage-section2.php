<section id="home-section-2">
    <div class="features position-relative">
        <?php if( have_rows('second_section') ): while( have_rows('second_section') ): the_row(); ?>
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12 p-0">
                        <div class="feature-info h-100" data-aos="fade-up" data-aos-delay="250" data-aos-duration="900">
                            <span class="title d-block type-m2 type-uppercase"><?php echo get_sub_field('title_text');?></span>
                            <p class="description mb-0 type-h3"><?php echo get_sub_field('description');?></p>
                            <?php $link_to = get_sub_field('link_to'); if( $link_to ) : ?>
                                <?php foreach( $link_to as $post ): setup_postdata($post); ?>
                                    <a href="<?php the_permalink();?>" class="w-100 type-m1 type-uppercase">
                                        Learn More <button class="btn-arrow"><i class="fas fa-chevron-right"></i></button>
                                    </a>
                                <?php break; endforeach; wp_reset_postdata(); ?>
                            <?php else : endif; ?>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 p-0"></div>
                </div>
            </div>
            <div class="container-fluid position-absolute">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12 p-0"></div>
                    <div class="col-lg-6 col-md-6 col-sm-12 p-0" data-aos="fade-up" data-aos-delay="100" data-aos-duration="900">
                        <div class="background-container" style="background-image: url(<?php echo get_sub_field('image');?>)"></div>
                    </div>
                </div>
            </div>
        <?php endwhile; endif; ?>
    </div>
</section>

