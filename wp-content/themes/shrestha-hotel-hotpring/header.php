<?php wp_head(); ?>
<html>

<head>
    <title>Shrestha Hotel Hotspring</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body <?php body_class(); ?>>

    <header>
        <nav class="navbar fixed-top navbar-expand-lg navbar-light">
            <div class="container" data-aos="fade-zoom-in" data-aos-delay="1500">
                <a href="<?php echo get_home_url();?>" class="site-logo position-relative">
                    <img class="main-logo" src="<?php echo get_template_directory_uri();?>/assets/images/logo.png" alt="Shrestha Hotel Hotspring"/>
                    <img class="alt-logo" src="<?php echo get_template_directory_uri();?>/assets/images/emblem.png" alt="Shrestha Hotel Hotspring"/>
                </a>

                <a href="<?php echo get_home_url();?>" class="site-emblem">
                    <img src="<?php echo get_template_directory_uri();?>/assets/images/emblem.png" alt="Shrestha Hotel Hotspring"/>
                </a>

                <span class="type-m1 type-uppercase site-title"><?php echo bloginfo('name');?></span>
        
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                
                <div class="collapse navbar-collapse ml-auto justify-content-end" id="navbarSupportedContent">
                    <?php wp_nav_menu(
                            array(
                                'menu' => 'main-menu',
                                'container'=>'ul',
                                'menu_class'=>"navbar-nav main-menu type-m1 type-uppercase",
                            )
                    ); ?>
                </div>
            </div>
        </nav>
    </header>