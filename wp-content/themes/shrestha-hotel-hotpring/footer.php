<?php wp_footer(); ?>

        <footer>
            <div class="container">

                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                        <div class="row">
                            <div class="col-sm-6 col-12 sub-widgets">
                                <?php if(dynamic_sidebar('footer_one')) : else : endif; ?>
                            </div>
                            <div class="col-sm-6 col-12 sub-widgets">
                                <?php if(dynamic_sidebar('footer_two')) : else : endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-7 col-sm-12 col-12 newsletter">
                        <?php if(dynamic_sidebar('footer_three')) : else : endif; ?>
                    </div>
                </div>

                <div class="row mt-4">
                    <?php wp_nav_menu(
                            array(
                                'menu' => 'footer-menu',
                                'container'=>'ul',
                                'menu_class'=>"type-m1 type-uppercase",
                            )
                    ); ?>
                </div>

                <div class="row mt-4">
                    <div class="copyright">
                        <?php if(dynamic_sidebar('footer_copyright')) : else : endif; ?>
                    </div>
                </div>

            </div>
        </footer>
    </body>
</html>