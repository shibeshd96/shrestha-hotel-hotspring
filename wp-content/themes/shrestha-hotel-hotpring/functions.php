<?php

// for meta tag for viewport
function add_viewport_meta_tag()
{
  echo '<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">';
}
add_action('wp_head', 'add_viewport_meta_tag', '1');


// =======================================================================

// loading css stylesheets
function load_stylesheets()
{

  wp_register_style('bootstrap', 'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.1.2/css/bootstrap.min.css', array(), false, 'all');
  wp_enqueue_style('bootstrap');

  wp_register_style('fontawesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.2.0/css/all.min.css', array(), false, 'all');
  wp_enqueue_style('fontawesome');

  wp_register_style('aos_css', 'https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.4/aos.css', array(), false, 'all');
  wp_enqueue_style('aos_css');

  wp_register_style('stylesheet', get_template_directory_uri() . '/assets/css/style.css', array(), false, 'all');
  wp_enqueue_style('stylesheet');

}
add_action('wp_enqueue_scripts', 'load_stylesheets');


// =======================================================================


//loading JS scripts
function load_js()
{

  wp_deregister_script('jquery');

  wp_register_script('jquery', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js', '', 1, true);
  wp_enqueue_script('jquery');

  wp_register_script('popperjs', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js', '', 1, true);
  wp_enqueue_script('popperjs');

  wp_register_script('bootstrapjs', 'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/js/bootstrap.min.js', '', 1, true);
  wp_enqueue_script('bootstrapjs');

  wp_register_script('aos_js', 'https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.4/aos.js', '', 1, true);
  wp_enqueue_script('aos_js');

  wp_register_script('customjs', get_template_directory_uri() . '/assets/js/script.js', '', 1, true);
  wp_enqueue_script('customjs');

}
add_action('wp_enqueue_scripts', 'load_js');


// =======================================================================


// To add thumbnail feature
add_theme_support('post-thumbnails');


// =======================================================================

// Custom Image Sizes
add_image_size( 'long-thumbnails', 450, 750, true );

// =======================================================================

// add menu support
function register_my_menus()
{
  register_nav_menus(
    array(
      'header-menu' => __('Header Menu'),
      'footer-menu' => __('Footer Menu')
    )
  );
}
add_action('init', 'register_my_menus');

// =======================================================================

// Use Old Wordpress Editor
add_filter('use_block_editor_for_post', '__return_false', 10);

// =======================================================================

// To Change Default Excerpt Length

function custom_excerpt_length( $length ) {
  return 23;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

// =======================================================================

// Custom Widget Area
function custom_widgets_init() {
    register_sidebar(
      array(
        'name' => 'Site Logo',
        'id' => 'site_logo',
        'before_widget' => '<aside class="site-logo">',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>'  
      )
    );
  
    register_sidebar(
      array(
        'name' => 'Footer Section One',
        'id' => 'footer_one',
        'before_widget' => '<aside>',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>'  
      )
    );
  
    register_sidebar(
      array(
        'name' => 'Footer Section Two',
        'id' => 'footer_two',
        'before_widget' => '<aside>',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>'  
      )
    );
  
    register_sidebar(
      array(
        'name' => 'Footer Section Three',
        'id' => 'footer_three',
        'before_widget' => '<aside>',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>'  
      )
    );
  
    register_sidebar(
      array(
        'name' => 'Footer Copyright',
        'id' => 'footer_copyright',
        'before_widget' => '<aside>',
        'after_widget' => '</aside>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>'  
      )
    );

}
add_action('widgets_init', 'custom_widgets_init');
  
// =======================================================================

// Custom Post Type
function custom_post_type() {

  $args = array(
    'labels' => array(
      'name' => 'Rooms',
      'singular' => 'room',
    ),
    'hierarchical' => 'true',
    'public' => true,
    'has_archive' => true,
    'menu_icon' => 'dashicons-building',
    'supports' => array('title', 'thumbnail', 'editor'),
  );
  register_post_type('room', $args);
}
add_action('init', 'custom_post_type');

//Taxonomy For Post Types
// function custom_taxonomy() {
  
//     // For Faculty
//     $args = array( 
//         'labels' => array (
//             'name' => 'Faculty-Category',
//             'singular' => 'Item',
//         ),
//         'hierarchical' => 'true',
//         'public' => true,
//         'has_archive' => true,
//         'supports' => array('title', 'thumbnail'), 
  
//     );
  
//     register_taxonomy('faculties', array('faculty'), $args);
  
// }
// add_action('init', 'custom_taxonomy');